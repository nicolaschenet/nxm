/*global module, require, __dirname*/

let Autoprefixer = require('autoprefixer');
let ExtractTextPlugin = require('extract-text-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');
let DefinePlugin = require('webpack').DefinePlugin;
let UglifyJsPlugin = require('webpack').optimize.UglifyJsPlugin;
let DedupePlugin = require('webpack').optimize.DedupePlugin;

let glob = require(`glob`);
let path = require(`path`);

let nxmCorePath = __dirname;
let applicationPath = process.env.NXM_APP_PATH;

module.exports = Object.assign({}, {

    resolveLoader: {

        root: `${nxmCorePath}/node_modules/`

    },

    resolve: {

        extensions: [ ``, `.js` ],

        modules: [

            `${nxmCorePath}/node_modules/`,

            nxmCorePath,
            applicationPath

        ],

        alias: require(`glob`).sync(`${applicationPath}/sources/*/`).reduce((alias, entry) => Object.assign(alias, {

            [entry]: `${applicationPath}/sources/${entry}/`

        }), {

            [`nxm`]: `${nxmCorePath}/nxm`,

            [`sources`]: `${applicationPath}/sources`,
            [`styles`]: `${applicationPath}/styles`

        })

    },

    entry: {

        [`main`]: [

            `babel-polyfill`,
            `expose?React!react`,

            `nxm/styles/index.scss`,
            `nxm/sources/index.js`

        ]

    },

    output: {

        path: `${applicationPath}/build/`,
        publicPath: `/react/`,

        filename: `[name].js`,
        chunkFilename: `[id].js`

    },

    devServer: {

        historyApiFallback: {
            index: `/react/`
        }

    },

    module: {

        loaders: [ {
            test: /\.js$/,
            loader: `babel-loader`,
            include: [ `${nxmCorePath}/nxm/sources`, `${applicationPath}/sources` ],
            query: { extends: `${nxmCorePath}/babel.config.json` }
        }, {
            test: /\.json$/,
            loader: `json-loader`,
            include: [ `${nxmCorePath}/nxm/sources`, `${applicationPath}/sources` ]
        }, {
            test: /\.scss$/,
            loader: `style-loader!css-loader!postcss-loader!sass-loader`
        }, {
            test: /\.svg$/,
            loader: `raw-loader`
        }, {
            test: /\.(gif|png|jpg|bmp|woff|webmanifest)$/,
            loader: `file-loader`
        } ]

    },

    externals: {

        [`fs`]: true

    },

    sassLoader: {

        data: `@import '~styles/auto';`

    },

    plugins: [

        new DefinePlugin({
            [`__PRODUCTION__`]: process.env.NODE_ENV === `production`,
            [`process.env.NODE_ENV`]: JSON.stringify(process.env.NODE_ENV)
        }),

        new HtmlWebpackPlugin({
            template: `nxm/assets/layout.ejs`,
            title: require(`${applicationPath}/package.json`).name
        })

    ]

});

if (process.env.NODE_ENV !== 'production') {

    module.exports.devtool = 'source-map';

} else {

    module.exports.plugins.push(new UglifyJsPlugin({ compress: { warnings: false } }));
    module.exports.plugins.push(new DedupePlugin());

}
